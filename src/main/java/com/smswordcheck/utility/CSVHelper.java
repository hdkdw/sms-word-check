package com.smswordcheck.utility;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.smswordcheck.dto.RWordData;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;



public class CSVHelper {
    private static String FILE_PATH = "C:\\Koding\\Java\\sms-word-check\\src\\main\\resources\\static\\rwordlist.csv";
   
    //convert csv file to List words object, source file : RWordData.csv using Apache commons (manually defined in dependency)    
    // parsing csv to be List of rwords

    public static List <RWordData> csvToRWordData (){

        // String filePath = "utility/rwordlist.csv";
        // File file = new File(filePath);
        // String FILE_PATH = file.getPath();
        try{

            BufferedReader fileReader = new BufferedReader(new FileReader(FILE_PATH));
            CSVParser parser = new CSVParser(fileReader, 
            CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim().withDelimiter(';'));

            List <RWordData> restrictedWords = new ArrayList<>(); //define variable list rWordDatas to store parsed csvrecords
            
            // String[] restrictedWords = new String[0];


            Iterable <CSVRecord> csvRecords = parser.getRecords();

            for (CSVRecord csvRecord: csvRecords){
                RWordData rWordData = new RWordData();
                rWordData.setRword(csvRecord.get("Rword"));
                restrictedWords.add(rWordData); // each rWordData added to restrictedWords
            }

            parser.close();
            return restrictedWords;



        }catch(IOException err){
            throw new RuntimeException("fail to parse CSV file containing rwords : " + err.getMessage());
        }

    }
    
    public static boolean smsWordCheckCSV(String smsInput){
        List <RWordData> rWordList = csvToRWordData();
        String result = "";
        
        for (int i=0 ; i < rWordList.size(); i++){
            result = result+"|"+ rWordList.get(i).getRword();
        }
        String regex = ".*\\b("+result.substring(1, result.length())+")\\b.*";


        return smsInput.matches(regex);
       
    }


}
