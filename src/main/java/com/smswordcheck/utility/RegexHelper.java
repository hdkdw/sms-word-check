package com.smswordcheck.utility;

import com.smswordcheck.entity.RestrictedWord;

public class RegexHelper {

   
    public static boolean smsWordCheck(String smsInput, Iterable<RestrictedWord> rWordList){
        
   
        String result = "";

        for (RestrictedWord element : rWordList) {
            result = result+"|"+element.getrWords();
        }

        String regex = "(?i).*\\b("+result.substring(1, result.length())+")\\b.*";

        return smsInput.matches(regex);
       
    }
    
}
