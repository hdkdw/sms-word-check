package com.smswordcheck.dto;

public class RWordData {
    

    private Long id;

    private String rword;

    public RWordData() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRword() {
        return rword;
    }

    public void setRword(String rword) {
        this.rword = rword;
    }

    

}
