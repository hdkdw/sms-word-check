package com.smswordcheck.dto;

public class ResponseData {
    
    private Long msgId;

    private String status;

    private String timestamp;

    public Long getMsgId() {
        return msgId;
    }

    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String string) {
        this.status = string;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String string) {
        this.timestamp = string;
    }

    



}
