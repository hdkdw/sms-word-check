package com.smswordcheck.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


import com.smswordcheck.dto.ReqInputSms;
import com.smswordcheck.dto.ResponseData;
import com.smswordcheck.entity.RestrictedWord;
import com.smswordcheck.service.RestrictedWordService;

import com.smswordcheck.utility.RegexHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class SmsWordController {
    
    @Autowired
    private RestrictedWordService restrictedWordService;


    @PostMapping("/uploadrwords")
    public ResponseEntity<RestrictedWord> create (@RequestBody RestrictedWord restrictedWord){
        
        restrictedWordService.save(restrictedWord);
        return ResponseEntity.ok().body(restrictedWord);
    }
    

    @PostMapping("/inputsms")
    public ResponseEntity<?> smsCheckWording(@RequestBody ReqInputSms reqInputSms){
        ResponseData respons = new ResponseData();
        Iterable<RestrictedWord> rWordList =restrictedWordService.findAll();

        if (RegexHelper.smsWordCheck(reqInputSms.getMessage(), rWordList)){
            LocalDateTime dateTime = LocalDateTime.now();
            DateTimeFormatter timeStamp = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
            respons.setMsgId(reqInputSms.getId());
            respons.setStatus("invalid");
            respons.setTimestamp(dateTime.format(timeStamp));
            return ResponseEntity.ok(respons);

        } else {
            LocalDateTime dateTime = LocalDateTime.now();
            DateTimeFormatter timeStamp = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
            respons.setMsgId(reqInputSms.getId());
            respons.setStatus("valid");
            respons.setTimestamp(dateTime.format(timeStamp));
            return ResponseEntity.ok(respons);
        }
    }


    @GetMapping("/getrwords")
    public ResponseEntity<?> listWord (){   //public ResponseEntity<List<RWordData>> listWord (){
        Iterable<RestrictedWord> rWordList =restrictedWordService.findAll();

          
        return ResponseEntity.ok().body(rWordList);
        
    }


}
