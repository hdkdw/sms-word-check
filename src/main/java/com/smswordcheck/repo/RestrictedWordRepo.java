package com.smswordcheck.repo;

import com.smswordcheck.entity.RestrictedWord;

import org.springframework.data.repository.CrudRepository;

public interface RestrictedWordRepo extends CrudRepository <RestrictedWord, Long> {
    
}
