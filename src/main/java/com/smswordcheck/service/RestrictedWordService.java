package com.smswordcheck.service;

import javax.transaction.Transactional;

import com.smswordcheck.entity.RestrictedWord;
import com.smswordcheck.repo.RestrictedWordRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class RestrictedWordService {
    
    @Autowired
    private RestrictedWordRepo restrictedWordRepo;


    public RestrictedWord save (RestrictedWord restrictedWord){
        return restrictedWordRepo.save(restrictedWord);
    }


    public Iterable<RestrictedWord> findAll (){
        return restrictedWordRepo.findAll();
    }



}
