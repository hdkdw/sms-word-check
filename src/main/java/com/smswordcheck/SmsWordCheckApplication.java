package com.smswordcheck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmsWordCheckApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmsWordCheckApplication.class, args);
	}

}
 